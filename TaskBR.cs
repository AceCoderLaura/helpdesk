
var taskInfoTransformed = (Adapt.Model.TaskManagement.Custom.TaskInfoCustom) record;

foreach (var cost in taskInfoTransformed.Costs)
{
	if (cost.StaffMember == null)
	{
		cost.StaffMember = DataAccessLayer.GetUser(connectionInfo.AuthToken);
	}
}

TaskInfoCustom oldTask = null;
if (taskInfoTransformed.TaskInfoKey != Guid.Empty)
{
	//Load the old task if there is one
	var oldTaskDataQuery = new DataQuery
	{
		SelectColumns =
		{
			TaskInfoCustom.ColumnTaskInfoKey,
			TaskInfoCustom.ColumnProbInsp,
			TaskInfoCustom.ColumnProbCode,
			TaskInfoCustom.ColumnTitle,
		},
		WhereClauses =
		{
			new WhereClause
			{
				LeftExpression = TaskInfo.ColumnTaskInfoKey,
				RightExpression = taskInfoTransformed.TaskInfoKey
			}
		}
	};

	//Note: Don't use the connectionInfo pass in here because otherwise this record will be cached and loaded later as the record for history tracking which triggers too many history records to be created.
	oldTask = DataAccessLayer<TaskInfoCustom>.GetRecords(oldTaskDataQuery, new ConnectionInfo(null, connectionInfo.AuthToken)).FirstOrDefault();
}

if ((oldTask == null || (oldTask != null && !string.IsNullOrEmpty(oldTask.Title))) && string.IsNullOrEmpty(taskInfoTransformed.Title))
{
	DataModelHelper.AddError(taskInfoTransformed, "Please specify a Title", "Title");
}

if (taskInfoTransformed.ProbAddDttm == DateTime.MinValue)
	taskInfoTransformed.ProbAddDttm = DateTime.Now;

/*If the job has no JobComplete set nad has a probinsp, then it is always just set to assigned*/
if (taskInfoTransformed.ProbInsp != null && taskInfoTransformed.JobCompleted == DateTime.MinValue)
{
	taskInfoTransformed.StatusKey = TaskManagementBusinessLayer.GetStatus("Assigned", connectionInfo);
}

/*If the job has ProbAck (started) set no JobComplete set and has a probinsp, then it is In Progress*/
if (taskInfoTransformed.ProbInsp != null && taskInfoTransformed.ProbAck != DateTime.MinValue && taskInfoTransformed.JobCompleted == DateTime.MinValue)
{
	taskInfoTransformed.StatusKey = TaskManagementBusinessLayer.GetStatus("In Progress", connectionInfo);
}

/*If the job has no JobComplete set and no probinsp then  just set to Unassigned*/
if (taskInfoTransformed.ProbInsp == null && taskInfoTransformed.JobCompleted == DateTime.MinValue)
{
	taskInfoTransformed.StatusKey = TaskManagementBusinessLayer.GetStatus("Unassigned", connectionInfo);
}

/*When job complete, it's set to closed and considered off the list*/
if (taskInfoTransformed.JobCompleted != DateTime.MinValue)
{
	taskInfoTransformed.StatusKey = TaskManagementBusinessLayer.GetStatus("Closed", connectionInfo);
}

//Code is complete and tested but not checked in to Source Control
if (taskInfoTransformed.IsCompletePendingCheckIn)
{
	taskInfoTransformed.StatusKey = TaskManagementBusinessLayer.GetStatus("Pending Checkin", connectionInfo);
}

if (taskInfoTransformed.Notes != null)
{
	foreach (var aNote in taskInfoTransformed.Notes)
	{
		if (aNote.NoteDate == DateTime.MinValue)
			aNote.NoteDate = DateTime.Now;
	}
}

/*Mandatory Fields for a task*/
if (taskInfoTransformed.ErrorDetails == null)
{
	taskInfoTransformed.ErrorDetails = new ErrorDictionary();
}

if (string.IsNullOrEmpty(taskInfoTransformed.ProbNotes))
{
	taskInfoTransformed.ErrorDetails.Add("ProbNotes", new List<ErrorDetail>
		{
			new ErrorDetail("Details of the task are required", "ProbNotes", false)
		});
}

/*When a job has the completed date set, make sure a comment for the closure is also made*/
if (taskInfoTransformed.JobCompleted != DateTime.MinValue && string.IsNullOrEmpty(taskInfoTransformed.ClosedComment))
{
	taskInfoTransformed.ErrorDetails.Add("JobCompleted", new List<ErrorDetail>
		{
			new ErrorDetail("When the job completed date is entered, you also need to comment on the result of the task in the Completed Comment field", "Completed Comment", false)
		});
}


/*If the completed comments are entered, but the job completed date is not yet set tell the user they cannot comment until it is closed*/
if (taskInfoTransformed.JobCompleted == DateTime.MinValue && !string.IsNullOrEmpty(taskInfoTransformed.ClosedComment))
{
	taskInfoTransformed.ErrorDetails.Add("Completed Comment", new List<ErrorDetail>
		{
			new ErrorDetail("If entering a completed comment, you must also set the completed date", "Completed Comment", false)
		});
}


/*Contract needs to be set*/
if (taskInfoTransformed.Contractor == null)
{
	taskInfoTransformed.ErrorDetails.Add("Client", new List<ErrorDetail>
		{
			new ErrorDetail("Need to set the client. If not Adapt, costs should be added where applicable", "Client", false)
		});
}

/*If contractor field is set to Adapt (via email), then default in the client instance as all.*/
if (taskInfoTransformed.Contractor != null)
{
	if (taskInfoTransformed.Contractor.CodeKey == 4)
	{
		if (taskInfoTransformed.ClientInstance == null)
		{
			taskInfoTransformed.ClientInstance = new Adapt.Model.Custom.ClientInstanceDef(30);
		}
	}
}

/*Client instnace needs to be set. But this stage it will be have been defaulted if the client was Adapt, but now check when others are used*/
if (taskInfoTransformed.ClientInstance == null)
{
	taskInfoTransformed.ErrorDetails.Add("Client Instance", new List<ErrorDetail>
		{
			new ErrorDetail("Need to set the client instnace", "ClientInstance", false)
		});
}

/*Create a new note based on the ActNotes field being filled in on the task*/
if (!string.IsNullOrEmpty(taskInfoTransformed.ActNotes))
{
	var note = new TaskNoteCustom
	{
		//Chat note type
		NoteCode = new NoteDef(47),
		NoteDate = DateTime.Now,
		NoteDesc = taskInfoTransformed.ActNotes
	};
	taskInfoTransformed.ActNotes = null;
	taskInfoTransformed.Notes.Add(note);
}

/*Set StaffMember to user if null*/


/*On Closing, check if we need costs added*/
//if (taskInfoTransformed.Contractor.CodeKey != 4 && taskInfoTransformed.JobCompleted != DateTime.MinValue)
//{
//	taskInfoTransformed.ErrorDetails.Add("Costs", new List<ErrorDetail>
//	{
//		new ErrorDetail("Check if costs need to be allocated to this job since it is not an 'Adapt' related task", "Costs", true)
//	});
//}

/*Implement the core business rule to audit all task changes and write them to the history table
This rule is to be at the end of the rule so it gets all user and system changes when it fires*/
Adapt.Business.Rules.HistoryTracking.UpdateHistory(record, connectionInfo, null, new List<string>
	{
		"Costs", "Notes"
	});


#if(!SILVERLIGHT)

//Create a background worker
var processNotificationsBackgroundWorker = new BackgroundWorker();

var getRecipientEmailAddresses = new Func<IEnumerable<UserAndUserPreferences>, IRecord, IEnumerable<UserAndUserPreferences>>((userAndUserPreferences, theRecord) =>
	{
		var taskInfo = theRecord as TaskInfo;
		return userAndUserPreferences.Where(u => taskInfo.ProbPriority == null || u.UserPreferences.NotificationPreferences.TaskNotificationPreferences.PriorityKeys.Contains(taskInfo.ProbPriority.CodeKey));
	});

//Handle the event
processNotificationsBackgroundWorker.DoWork += (sender, e) =>
{
	try
	{
		var recordUpdatedInfo = (RecordUpdatedInfo) e.Argument;
		var task = (TaskInfoCustom) recordUpdatedInfo.Record;

		if (task.HasErrors)
		{
			//The task was not saved
			return ;
		}

		//The transaction wrapper probably has not finished closing the transaction at this point
		//But, it will probably have called Commit, so any attempt to use the transaction will fail. Need to wait for the transaction to finish
		//Really, we need a separate Business Rule event for after the Save transaction has been committed.
		Thread.Sleep(5000);

		//Get the subscriptions for the record
		Notifications.ProcessRecordSubscriptions(recordUpdatedInfo, task.ExchangeConversationId);

		Logger.Log("ProcessRecordSubscriptions completed", LogMessageType.Information);

		if (oldTask == null || (oldTask.ProbInsp != null && oldTask.ProbInsp.UserKey != task.ProbInsp.UserKey))
		{
			//Load the new task menu item so that we can get the email template
			var menuItem = DataAccessLayer<MenuItem>.GetRecord(new Guid("d4afd2cf-2368-400e-aa56-e45950924793"), recordUpdatedInfo.ConnectionInfo);

			//Send the notification to the new inspector if they have elected to receive notifications about this priority type
			Notifications.SendNotifications(recordUpdatedInfo, task.ExchangeConversationId, menuItem, new List<User>
				{
					task.ProbInsp
				}, getRecipientEmailAddresses);
		}

		//Dispose of the worker
		var backgroundWorker = (BackgroundWorker) sender;
		backgroundWorker.Dispose();
	}
	catch (Exception ex)
	{
		Logger.Log(ex);
	}
};

//Run the worker in the background
processNotificationsBackgroundWorker.RunWorkerAsync(new RecordUpdatedInfo(record, connectionInfo));
#endif
